function postUser(parent, args, context, info) {
  return context.prisma.createUser({
    name: args.name
  });
}
// function postMessage(parent, args, context, info) {
//   return context.prisma.createMessage({
//     text: args.text,
//     isAnswer: args.isAnswer,
//     likesCount: 0,
//     dislikesCount: 0
//   });
// }

module.exports = {
  postUser
  // postMessage
};